<?php
/**
 * Author: Twosee <twose@qq.com>
 * Date: 2018/6/29 下午11:29
 */

namespace Swlib\Util;

trait SingletonTrait
{
    private static $instance;

    public static function getInstance(...$args): self {
        if (!isset(self::$instance)) {
            /** @noinspection PhpMethodParametersCountMismatchInspection */
            self::$instance = new static(...$args);
        }
        return self::$instance;
    }

}
